package ru.ekfedorov.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.command.AbstractCommand;
import ru.ekfedorov.tm.exception.system.NullObjectException;

public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        System.out.println("[VERSION]");
        System.out.println(Manifests.read("version"));
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

}
