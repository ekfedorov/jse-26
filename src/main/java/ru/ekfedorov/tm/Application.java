package ru.ekfedorov.tm;

import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.util.SystemUtil;

public class Application {

    public static void main(final String[] args) throws Exception {
        System.out.println("PID: " + SystemUtil.getPID());
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
